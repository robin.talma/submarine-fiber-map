#!/bin/bash

url="https://tiles.telegeography.com/maps/submarine-cable-map-2015/6/"

sudo apt install imagemagick -y
sudo cp policy.xml /etc/ImageMagick-6/policy.xml

mkdir png

for x in {0..63}
do
for y in {12..53}
  do
    curl -s $url$x/$y.png -o png/map-$x-$y.png &
    echo "Get X = $x AND Y = $y "
 done
sleep 1
done

sleep 3

for y in {0..63}
do
  echo "Append vertically $y/63"
  convert -append png/map-$y-{12..53}.png png/v-$y.png
done

echo "Append horizontally"
convert +append png/v-{0..63}.png Submarine_Cable_Map_2015.png
rm -r png

echo "[Done.]"
